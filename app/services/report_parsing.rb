class ReportParsing
	def initialize(data)
		@data = data
		parsing_data
	end

	def parsing_data
		if @data.raw_notes.include?(Note.stage[0])
      stage_two
    elsif @data.raw_notes.include?(Note.stage[1])
      stage_three
    else
     stage_one    
    end 
	end

	def stage_two
		stage = "two"
    formatted_note       =  @data.raw_notes.gsub("\r","")
    notes_array          =  formatted_note.split("\n")
    sorted_array         =  notes_array.reject{|c|c.blank?}
    heading              =  sorted_array[0]
    test_type            =  sorted_array[1]&.split(":")&.last
    result               =  sorted_array.last&.split(":")&.last
    patient_performance  =  ""
    for i in 2..sorted_array.length-2
      patient_performance << sorted_array[i] + "|"
    end

    @data.update(test_type: test_type, patient_performance: patient_performance, heading: heading, test_type: test_type, result: result, stage: "two")
	end


	def stage_one
	  stage                = "one"
    formatted_note       =  @data.raw_notes.gsub("\r","")
    notes_array          =  formatted_note.split("\n")
    sorted_array         =  notes_array.reject{|c|c.blank?}
    error                = sorted_array.last.split(":").last
    # get test type
    char_plus          = @data.raw_notes.index("+")
    char_minus         = @data.raw_notes.index("-")

    if char_plus > char_minus
      number = char_minus
    else
      number = char_plus
    end  
    test_type = @data.raw_notes[0, number]
    index_of_percentage = @data.raw_notes.index("%")
    patient_performance_array = @data.raw_notes[number..index_of_percentage].split(" ")
    patient_performance = patient_performance_array[0]
    result = ""
    for i in 1..patient_performance_array.length-1
      result << patient_performance_array[i]
    end

    @data.update(stage: "one",test_type: test_type, patient_performance: patient_performance, stage: stage, test_type: test_type, result: result, error: error)
	end

	def stage_three
		formatted_note       =  @data.raw_notes.gsub("\r","")
    notes_array          =  formatted_note.split("\n")
    sorted_array         =  notes_array.reject{|c|c.blank?}
    test_type            =  sorted_array[0].split(":").last
    result               =  sorted_array.last.split(":").last
    n_performance        =  sorted_array[1].split(":").last
    n_error              =  sorted_array[2].split(":").last
    v_performance        =  sorted_array[3].split(":").last
    v_error              =  sorted_array[4].split(":").last
    @data.update(test_type: test_type, stage: "three", test_type: test_type, result: result, n_error: n_error, v_error: v_error, n_performance: n_performance, v_performance: v_performance)
	end
end 
