json.extract! note, :id, :patient_id, :therapist_id, :error, :patient_performance, :result, :n_performance, :n_error, :n_result, :v_performance, :v_error, :v_result, :stage, :raw_notes, :created_at, :updated_at
json.url note_url(note, format: :json)
