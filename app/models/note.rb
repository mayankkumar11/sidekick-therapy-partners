class Note < ApplicationRecord
  belongs_to :therapist
  belongs_to :patient
  validates  :patient, presence: true
  validates  :therapist, presence: true
  validates  :raw_notes, presence: true


  def arrange_report
    ReportParsing.new(self)
  end

  def self.stage
    ["STG2", "STG3"]
  end
end