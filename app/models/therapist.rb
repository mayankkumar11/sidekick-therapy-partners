class Therapist < ApplicationRecord
	has_many :notes
	has_many :patients, through: :notes
end
