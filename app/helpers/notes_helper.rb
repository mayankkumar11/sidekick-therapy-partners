module NotesHelper
	def structure_data
    patient_performance = @note.patient_performance
    patient_performance_array = patient_performance.split("|")
    view_array = []
    patient_performance_array.each do |patient_word|
    	hash  = {}
    	hash[patient_word.split(" ").first] = patient_word.split(" ").last
    	view_array << hash
    end
    view_array	
	end

	def get_result(data)
		view_string = ""
		for i in 0..data.length()-1
			string = ""
      string = (data[i]=="+" ? "success" : "unsuccess")
			view_string << string + " "
		end
		view_string	
	end

	def get_noun_result(string)
		if string.present? 
			array  = string.split(" ")
			array.last
		end	
	end

  def get_noun_description(string)
  	array  = string.split(" ")
  	performance = ""
  	for i in 0..array.length-2
      performance << array[i]
		end
		description = performance.gsub(/\s+/, "")
		description_array = []
		for i in 0..description.length-1
			description_array << (description[i]=="+" ? "success" : "unsuccess")
		end
		description_array
  end

  def stage_one_error(string)
  	if string.present?
  		string.split(",")
  	end	
  end	
end