Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html
  root "notes#index"
  resources :notes
  # Defines the root path route ("/")
  # root "articles#index"
end
