# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Therapist.create(name: "Sam", therapist_number: "thera_123")
Therapist.create(name: "Tom", therapist_number: "thera_456")
Patient.create(name: "John", patient_number: "9893")
Patient.create(name: "Bobby", patient_number: "5853")