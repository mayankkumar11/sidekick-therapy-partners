class CreateNotes < ActiveRecord::Migration[7.0]
  def change
    create_table :notes do |t|
      t.integer :patient_id
      t.integer :therapist_id
      t.string :error
      t.string :patient_performance
      t.string :result
      t.string :n_performance
      t.string :n_error
      t.string :v_performance
      t.string :v_error
      t.string :stage
      t.text   :raw_notes
      t.string :heading
      t.string :test_type
      t.timestamps
    end
  end
end
